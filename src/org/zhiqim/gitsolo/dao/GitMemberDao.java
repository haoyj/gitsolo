/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.dao;

import java.sql.SQLException;
import java.util.List;

import org.zhiqim.gitsolo.dbo.GitMember;
import org.zhiqim.gitsolo.dbo.GitMemberEx;
import org.zhiqim.gitsolo.dbo.GitReport;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.util.Ids;
import org.zhiqim.kernel.util.Sqls;
import org.zhiqim.kernel.util.Strings;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ORMException;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.ZView;
import org.zhiqim.orm.dbo.Selector;

/**
 * 项目成员数据访问对象
 *
 * @version v1.0.0 @author zouzhigang 2017-11-9 新建与整理
 */
@AnAlias("GitMemberDao")
public class GitMemberDao
{
    /**
     * 获取当前请求的成员列表
     * 
     * @param request       请求
     * @return              模块列表
     * @throws Exception    异常
     */
    public static List<GitMember> list(HttpRequest request) throws Exception
    {
        Selector selector = new Selector();
        selector.addMust("projectId", GitProjectDao.getProjectId(request));
        selector.addOrderbyAsc("memberType");
        
        return ORM.get(ZTable.class, request).list(GitMember.class, selector);
    }
    
    /**
     * 获取当前请求的成员扩展列表
     * 
     * @param request       请求
     * @return              模块列表
     * @throws Exception    异常
     */
    public static List<GitMemberEx> listx(HttpRequest request) throws Exception
    {
        Selector selector = new Selector();
        selector.addMust("projectId", GitProjectDao.getProjectId(request));
        selector.addOrderbyAsc("memberType");
        
        return ORM.get(ZView.class, request).list(GitMemberEx.class, selector);
    }
    

    /**
     * 记录项目成员动态，指定功能和功能目标
     * 
     * @param request       请求对象
     * @param projectId     项目编号
     * @param operatorCode  操作员编码
     * @param featrue       功能描述
     * @param target        功能目标
     * @throws SQLException 数据库异常
     * @throws ORMException 数据映射异常
     */
    public static void report(HttpRequest request, long projectId, String operatorCode, String featrue, Object target) throws SQLException, ORMException
    {
        String reportDesc = featrue + " $" + Strings.toString(target);
        report(request, projectId, operatorCode, reportDesc);
    }
    
    /**
     * 记录项目成员动态，指定功能和功能目标
     * 
     * @param request       请求对象
     * @param projectId     项目编号
     * @param operatorCode  操作员编码
     * @param featrue       功能描述
     * @param target        功能目标
     * @throws SQLException 数据库异常
     * @throws ORMException 数据映射异常
     */
    public static void report(HttpRequest request, String operatorCode, long projectId, String featrue, Object target) throws SQLException, ORMException
    {
        String reportDesc = featrue + " @" + Strings.toString(target);
        report(request, projectId, operatorCode, reportDesc);
    }
    
    /**
     * 记录项目成员动态，指定操作和功能及功能目标
     * 
     * @param request       请求对象
     * @param operatorCode  操作员编码
     * @param projectId     项目编号
     * @param target        功能目标
     * @param featrue       功能描述
     * @param issueSeq      问题排序
     * @throws SQLException 数据库异常
     * @throws ORMException 数据映射异常
     */
    public static void report(HttpRequest request, String operatorCode, long projectId, String target, String featrue, int issueSeq) throws SQLException, ORMException
    {
        String reportDesc = target + " " + featrue + " #" + issueSeq;
        report(request, projectId, operatorCode, reportDesc);
    }
    
    /**
     * 记录项目成员动态，指定功能和功能目标及被操作人
     * 
     * @param request       请求对象
     * @param projectId     项目编号
     * @param operatorCode  操作员编码
     * @param featrue       功能描述
     * @param target        功能目标
     * @param member        被操作人
     * @throws SQLException 数据库异常
     * @throws ORMException 数据映射异常
     */
    public static void report(HttpRequest request, long projectId, String operatorCode, String featrue, int target, String member) throws SQLException, ORMException
    {
        String reportDesc = featrue + " #" + target + "   @" + member;
        report(request, projectId, operatorCode, reportDesc);
    }
    
    
    
    /**
     * 记录项目成员动态
     * 
     * @param request       请求对象
     * @param projectId     项目编号
     * @param operatorCode  操作员编码
     * @param reportDesc    动态信息
     * @throws SQLException 数据库异常
     * @throws ORMException 数据映射异常
     * */
    public static void report(HttpRequest request, long projectId, String operatorCode, String reportDesc) throws SQLException, ORMException
    {
        GitReport report = new GitReport();
        report.setProjectId(projectId);
        report.setOperatorCode(operatorCode);
        report.setReportId(Ids.longId());
        report.setReportTime(Sqls.nowTimestamp());
        report.setReportDesc(reportDesc);
        
        ORM.get(ZTable.class, request).insert(report);
    }
}
