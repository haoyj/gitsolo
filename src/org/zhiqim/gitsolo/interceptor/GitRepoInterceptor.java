/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.interceptor;

import java.util.ArrayList;
import java.util.List;

import org.zhiqim.git.Git;
import org.zhiqim.git.GitConstants;
import org.zhiqim.git.refer.Ref;
import org.zhiqim.git.util.Gits;
import org.zhiqim.gitsolo.Gitsolo;
import org.zhiqim.gitsolo.GitsoloConstants;
import org.zhiqim.gitsolo.dbo.GitRepository;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.core.Interceptor;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.util.Validates;
import org.zhiqim.manager.ZmrSessionUser;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;

/**
 * 检查后端仓库拦截器
 *
 * @version v1.0.0 @author zouzhigang 2017-12-5 新建与整理
 */
@AnAlias("chkGitRepo")
public class GitRepoInterceptor implements Interceptor, GitConstants, GitsoloConstants
{
    @Override
    public void intercept(HttpRequest request) throws Exception
    {
        long repositoryId = request.getParameterLong("repositoryId");
        GitRepository repo = ORM.get(ZTable.class, request).item(GitRepository.class, repositoryId);
        if(repo == null)
        {
            request.setRedirect("repository.htm", "该仓库没在数据库中");
            return;
        }
        
        ZmrSessionUser sessionUser = request.getSessionUser(ZmrSessionUser.class);
        long projectId = sessionUser.getOperatorParamLong(GIT_PROJECT_ID_KEY);
        if (repo.getProjectId() != projectId)
        {//切换了项目
            request.setRedirect("repository.htm");
            return;
        }
        
        String repositoryName = Gitsolo.getRepositoryPath(repo.getProjectId(), repo.getRepositoryCode());
        Git git =  Gits.git(repositoryName);
        if(git == null)
        {
            request.setRedirect("repository.htm", "该Git仓库未找到");
            return;
        }
        
        request.addParam("repositoryId", ""+repositoryId);
        request.setAttribute("repositoryName", repositoryName);
        request.setAttribute("GitRepository", repo);
        request.setAttribute(GIT_ATTRIBUTE_REPOSITORY, git);
        
        //把分枝和标签列表显示出来
        String head = request.getParameter("head");
        if (Validates.isEmpty(head))
            head = "master";
        
        List<String> headList = new ArrayList<>();
        
        headList.add("分枝");
        List<Ref> refList = git.branchList().call();
        for (Ref ref : refList)
        {
            String brancheName = ref.getName().substring(P_REFS_HEADS_.length());
            headList.add(brancheName);
        }
        
        headList.add("标签");
        List<Ref> tRefList = git.tagList().call();
        for (Ref ref : tRefList)
        {
            String tagName = ref.getName().substring(P_REFS_TAGS_.length());
            headList.add(tagName);
        }
        
        //把选中的放置第一个位置
        headList.add(0, head);
        request.setAttribute("headList", headList);
    }
}
