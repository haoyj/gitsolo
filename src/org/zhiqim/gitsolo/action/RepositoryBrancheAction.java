/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.action;

import java.util.ArrayList;
import java.util.List;

import org.zhiqim.git.Git;
import org.zhiqim.git.GitConstants;
import org.zhiqim.git.objer.objectid.ObjectId;
import org.zhiqim.git.objer.objectid.RevCommit;
import org.zhiqim.git.refer.Ref;
import org.zhiqim.gitsolo.dao.GitMemberDao;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.gitsolo.model.BrancheModel;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.StdSwitchAction;
import org.zhiqim.httpd.validate.ones.IsAlphaNumericUlPALen;

/**
 * 分枝列表
 *
 * @version v1.0.0 @author zouzhigang 2016-11-1 新建与整理
 */
public class RepositoryBrancheAction extends StdSwitchAction implements GitConstants
{
    @Override
    protected void validateId(HttpRequest request)
    {
        request.addValidate(new IsAlphaNumericUlPALen("brancheName", "分枝名称为字母数字和下划线组成，字母开头1-20位", 1, 20));
    }
    
    @Override
    protected void validateForm(HttpRequest request)
    {
        request.addValidate(new IsAlphaNumericUlPALen("brancheName", "分枝名称为字母数字和下划线组成，字母开头1-20位", 1, 20));
    }
    
    @Override
    protected void list(HttpRequest request) throws Exception
    {
        Git git = (Git)request.getAttribute(GIT_ATTRIBUTE_REPOSITORY);
        List<Ref> refList = git.branchList().call();
        
        List<BrancheModel> list = new ArrayList<>();
        for (Ref ref : refList)
        {
            ObjectId oid = ref.getObjectId();
            RevCommit commit = git.resolve(oid, RevCommit.class);
            list.add(new BrancheModel().setRef(ref).setCommit(commit));
        }
        
        request.setAttribute("list", list);
    }
    
    @Override
    protected void add(HttpRequest request) throws Exception
    {
    }
    
    @Override
    protected void insert(HttpRequest request) throws Exception
    {
        String brancheName = request.getParameter("brancheName");
        
        Git git = (Git)request.getAttribute(GIT_ATTRIBUTE_REPOSITORY);
        git.branchCreate().setForce(true).setName(brancheName).call();
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "创建了分支", brancheName);
    }
    
    @Override
    protected void modify(HttpRequest request) throws Exception
    {
    }

    @Override
    protected void update(HttpRequest request) throws Exception
    {
        String oldBrancheName = request.getParameter("oldBrancheName");
        String brancheName = request.getParameter("brancheName");
        if(oldBrancheName.equals(brancheName))
            return;
        
        Git git = (Git)request.getAttribute(GIT_ATTRIBUTE_REPOSITORY);
        List<Ref> refList = git.branchList().call();
        for (Ref ref : refList)
        {
            String brancheName1 = ref.getName().substring(P_REFS_HEADS_.length());
            if(brancheName1.equals(brancheName))
            {
                request.setResponseError("该分支已存在!");
                return;
            }
        }
        
        git.branchRename().setNewName(brancheName).setOldName(oldBrancheName).call();
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "修改了分支", brancheName);
    }
    
    @Override
    protected void delete(HttpRequest request) throws Exception
    {
        String brancheName = request.getParameter("brancheName");
        
        Git git = (Git)request.getAttribute(GIT_ATTRIBUTE_REPOSITORY);
        git.branchDelete().setBranchNames(brancheName).setForce(true).call();
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "删除了分支", brancheName);
    }
}
