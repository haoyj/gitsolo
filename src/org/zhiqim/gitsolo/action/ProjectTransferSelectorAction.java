/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.action;

import java.util.List;

import org.zhiqim.gitsolo.dbo.GitMemberEx;
import org.zhiqim.gitsolo.dbo.GitProject;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.core.Action;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.ZView;
import org.zhiqim.orm.dbo.Selector;

/**
 * 转让项目
 *
 * @version v1.0.0 @author huangsufang 2017-9-25 新建与整理
 */
public class ProjectTransferSelectorAction implements Action
{
    @Override
    public void execute(HttpRequest request) throws Exception
    {
        long projectId = request.getParameterLong("projectId");
        if(projectId == -1)
        {
            request.returnCloseDialog("请选择一个有效的项目");
            return;
        }
        
        GitProject project = ORM.get(ZTable.class, request).item(GitProject.class, projectId);
        if(project == null)
        {
            request.returnCloseDialog("请选择一个有效的项目");
            return;
        }
        
        Selector selector = new Selector();
        selector.addMust("projectId", projectId);
        selector.addMust("memberType", 1);
        List<GitMemberEx> memberList = ORM.get(ZView.class, request).list(GitMemberEx.class, selector);
       
        request.setAttribute("project", project);
        request.setAttribute("memberList", memberList);
    }
}

