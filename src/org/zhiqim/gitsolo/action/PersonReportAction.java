/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.action;

import org.zhiqim.gitsolo.GitsoloConstants;
import org.zhiqim.gitsolo.dbo.GitReader;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.core.Action;
import org.zhiqim.kernel.util.Validates;
import org.zhiqim.manager.dbo.ZmrOperator;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;

/**
 *  指定个人的项目动态
 *
 * @version v1.0.0 @author zouzhigang 2017-11-2 新建与整理
 */
public class PersonReportAction implements Action, GitsoloConstants
{
    @Override
    public void execute(HttpRequest request) throws Exception
    {
        String operatorCode = request.getParameter("operatorCode");
        if (Validates.isEmptyBlank(operatorCode))
        {
            request.returnHistory("请选择一个成员");
            return;
        }
        
        ZmrOperator operator = ORM.get(ZTable.class, request).item(ZmrOperator.class, operatorCode);
        if (operator == null)
        {
            request.returnHistory("请选择一个有效成员");
            return;
        }
        
        GitReader reader = ORM.get(ZTable.class, request).item(GitReader.class, operatorCode, request.getSessionName());
        if (reader == null)
        {
            request.returnHistory("请选择一个有权限的成员");
            return;
        }
        
        request.setAttribute("operator", operator);
        
        //生成个人项目动态
        MyReportAction.report(request, operatorCode);
    }
}
