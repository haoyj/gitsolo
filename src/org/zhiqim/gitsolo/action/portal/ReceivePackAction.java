/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.action.portal;

import java.io.IOException;

import org.zhiqim.git.Git;
import org.zhiqim.git.GitConstants;
import org.zhiqim.git.http.ReceiveHandler;
import org.zhiqim.git.objer.model.PersonIdent;
import org.zhiqim.gitsolo.Gitsolo;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.HttpResponse;
import org.zhiqim.httpd.context.core.Action;
import org.zhiqim.kernel.logging.Log;
import org.zhiqim.kernel.logging.LogFactory;
import org.zhiqim.manager.dbo.ZmrOperator;

/**
 * 接收包（客户端push）
 * 
 * @version v1.0.0 @author zouzhigang 2016-10-21 新建与整理
 */
public class ReceivePackAction implements Action, GitConstants
{
    private static final Log log = LogFactory.getLog(UploadPackAction.class);
    
    @Override
    public void execute(HttpRequest request) throws Exception
    {
        HttpResponse response = request.getResponse();

        if (!RECEIVE_PACK_REQUEST_TYPE.equals(request.getContentType()))
        {// 内容类型必须正确
            response.sendError(_415_UNSUPPORTED_MEDIA_TYPE_);
            return;
        }
        
        //请求正确，设置响应类型
        response.setContentTypeNoCharset(RECEIVE_PACK_RESULT_TYPE);
        
        //组装上传包数据
        Git git = (Git)request.getAttribute(GIT_ATTRIBUTE_REPOSITORY);
        ZmrOperator opr = (ZmrOperator)request.getAttribute(GIT_ATTRIBUTE_OPERATOR);

        ReceiveHandler handler = new ReceiveHandler(git);
        handler.setPersonIdent(new PersonIdent(opr.getOperatorCode(), opr.getOperatorEmail()));
        
        try
        {
            handler.execute(request.getInputStream(), response.getOutputStream());
        }
        catch(IllegalArgumentException e)
        {
            Gitsolo.sendError(response, handler, e.getMessage());
            log.error(e.getMessage());
        }
        catch (IOException e)
        {
            Gitsolo.sendError(response, handler, "解析接受包失败");
            log.error(e);
        }
        catch (Throwable e)
        {
            Gitsolo.sendError(response, handler, "处理接受包失败");
            log.error(e);
        }
    }
}
