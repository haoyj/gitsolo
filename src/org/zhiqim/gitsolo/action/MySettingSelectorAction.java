/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.action;

import java.util.Iterator;
import java.util.List;

import org.zhiqim.gitsolo.dbo.GitReader;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.core.Action;
import org.zhiqim.kernel.util.Validates;
import org.zhiqim.manager.dbo.ZmrOperator;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;

/**
 * 我的汇报对象选择器
 *
 * @version v1.0.0 @author zouzhigang 2017-11-7 新建与整理
 */
public class MySettingSelectorAction implements Action
{
    @Override
    public void execute(HttpRequest request) throws Exception
    {
        List<GitReader> readerList = ORM.get(ZTable.class, request).list(GitReader.class, new Selector("readerCode", "operatorCode", request.getSessionName()));
        
        Selector selector = new Selector();
        selector.addMust("operatorStatus", 0);
        selector.addMustNotEqual("operatorCode", request.getSessionName());
        String operatorKey = request.getParameter("operatorKey");
        if (Validates.isNotEmpty(operatorKey))
        {//条件
            selector.addOr(new Selector().addMaybeLike("operatorCode", operatorKey).addMaybeLike("operatorName", operatorKey));
        }
        
        List<ZmrOperator> operatorList = ORM.get(ZTable.class, request).list(ZmrOperator.class, selector);
        
        for (Iterator<ZmrOperator> it = operatorList.iterator(); it.hasNext();)
        {
            ZmrOperator operator = it.next();
            if (isContain(readerList, operator))
            {//如果是自己，或在汇报列表中则删除
                it.remove();
            }
        }
        
        request.setAttribute("operatorList", operatorList);
    }      
        
    private boolean isContain(List<GitReader> readerList, ZmrOperator operator)
    {
        for (GitReader reader : readerList)
        {
            if (operator.getOperatorCode().equals(reader.getReaderCode()))
                return true;
        }
        
        return false;
    }
        
}

