/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.presenter;

import java.util.List;

import org.zhiqim.gitsolo.GitsoloConstants;
import org.zhiqim.gitsolo.dao.GitMemberDao;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.gitsolo.dbo.GitMember;
import org.zhiqim.gitsolo.dbo.GitProject;
import org.zhiqim.gitsolo.dbo.GitReader;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.annotation.AnIntercept;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.httpd.validate.onex.IsInteger;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnTransaction;
import org.zhiqim.kernel.constants.CodeConstants;
import org.zhiqim.kernel.util.Lists;
import org.zhiqim.kernel.util.Sqls;
import org.zhiqim.manager.ZmrSessionUser;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;
import org.zhiqim.orm.dbo.Updater;

/**
 * 工程展示器
 *
 * @version v1.0.0 @author zouzhigang 2021-4-2 新建与整理
 */
@AnAlias("GitProjectPresenter")
@AnIntercept("chkZmrLogin")
public class GitProjectPresenter implements CodeConstants, GitsoloConstants
{
    /**
     * 设置操作员默认项目
     * 
     * @param request       请求对象
     * @param projectId     项目编号
     * @throws Exception    异常
     */
    public static void setProjectId(HttpRequest request, long projectId) throws Exception
    {
        ZmrSessionUser sessionUser = request.getSessionUser(ZmrSessionUser.class);
        GitProjectDao.setProjectId(request, sessionUser, projectId);
    }

    /**
     * 删除项目成员
     * 
     * @param request       请求对象
     * @param projectId     项目编号
     * @param operatorCode  操作员编码
     * @throws Exception    异常
     */
    public static void doDeleteMember(HttpRequest request, long projectId, String operatorCode) throws Exception
    {
        if (!GitProjectDao.chkProjectId(request, projectId))
            return;
        
        Selector sel = new Selector();
        sel.addMust("projectId", projectId);
        sel.addMust("projectManager", operatorCode);
        if (ORM.get(ZTable.class, request).count(GitProject.class, sel) > 0)
        {
            request.setResponseError("该成员为项目负责人，不能删除!");
            return;
        }
        
        Selector selector = new Selector();
        selector.addMust("projectId", projectId);
        selector.addMust("operatorCode", operatorCode);
        ORM.get(ZTable.class, request).delete(GitMember.class, selector);
        
        GitMemberDao.report(request,request.getSessionName(), projectId, "移除了成员", operatorCode);
    }
    
    /**
     * 批量增加项目成员
     * 
     * @param request       请求对象
     * @param projectId     项目编号
     * @param operatorCodes 操作员编码组
     * @throws Exception    异常
     */
    public static void doAddMembers(HttpRequest request, long projectId, String operatorCodes) throws Exception
    {
        if (!GitProjectDao.chkProjectId(request, projectId))
            return;
        
        List<String> memberList = Lists.toStringList(operatorCodes);
        for (String item : memberList)
        {
            GitMember member = new GitMember();
            member.setProjectId(projectId);
            member.setOperatorCode(item);
            member.setMemberType(1);//普通成员
            ORM.get(ZTable.class, request).replace(member);
            
            GitMemberDao.report(request,request.getSessionName(), projectId, "增加了成员", item);
        }
    }
    
    /**
     * 修改成员角色类型
     * 
     * @param projectId     项目编号
     * @param operatorCode  操作员编码
     * @param role          角色
     * @throws Exception    异常
     */
    public static void doRole(HttpRequest request ,long projectId, String operatorCode, String role)throws Exception
    {
        if (!GitProjectDao.chkProjectId(request, projectId))
            return;
        
        Selector selector = new Selector();
        selector.addMust("projectId", projectId);
        selector.addMust("operatorCode", operatorCode);
        GitMember member = ORM.get(ZTable.class, request).item(GitMember.class, selector);
        
        List<String> roleList = Lists.toStringList(member.getMemberRole());
        if (roleList.contains(role))
            roleList.remove(role);
        else
            roleList.add(role);
        
        String newRole = Lists.toString(roleList);
        
        Updater updater = new Updater();
        updater.addMust("projectId", projectId);
        updater.addMust("operatorCode", operatorCode);
        updater.addField("memberRole", newRole);
        ORM.get(ZTable.class, request).update(GitMember.class, updater);
        
        GitMemberDao.report(request, request.getSessionName(), projectId, "修改了成员权限", operatorCode);
    }
    
    /**
     * 项目转让
     * 
     * @param request       请求对象
     * @throws Exception    异常
     */
    @AnTransaction
    public static void doTransferProject(HttpRequest request) throws Exception
    {
        request.addValidate(new IsInteger("projectId", "请选择项目"));
        request.addValidate(new IsNotEmpty("operatorCode", "请选择转让的成员"));
        if (!request.chkValidate())
        {
            request.setResponseError(request.getAlertMsg());
            return;
        }
        
        long projectId = request.getParameterLong("projectId");
        String operatorCode = request.getParameter("operatorCode");
        
        GitProject project= ORM.get(ZTable.class, request).item(GitProject.class, projectId);
        if (project == null)
        {
            request.returnHistory("请选择一个有效的项目！");
            return;
        }
        
        //1.修改项目组长为新的成员
        Updater updater = new Updater();
        updater.addMust("projectId", projectId);
        updater.addField("projectManager", operatorCode);
        updater.addField("projectModified", Sqls.nowTimestamp());
        ORM.get(ZTable.class, request).update(GitProject.class, updater);
        
        //2.将原项目组长改为普通成员，且没有权限，如果需要权限需新的组长设置
        Updater updater1 = new Updater();
        updater1.addMust("projectId", projectId);
        updater1.addMust("operatorCode", project.getProjectManager());
        updater1.addField("memberType", 1);
        ORM.get(ZTable.class, request).update(GitMember.class, updater1);
        
        //3.将设置新的项目组长
        Updater updater2 = new Updater();
        updater2.addMust("projectId", projectId);
        updater2.addMust("operatorCode", operatorCode);
        updater2.addField("memberType", 0);
        updater2.addField("memberRole", null);
        ORM.get(ZTable.class, request).update(GitMember.class, updater2);
        
        GitMemberDao.report(request, projectId, request.getSessionName(), "转让了项目", project.getProjectName());
    }
    
    /***********************************************************************************************/
    //汇报增加&删除
    /***********************************************************************************************/
    
    /**
     * 批量增加汇报对象
     * 
     * @param request       请求对象
     * @param readerCodes   操作员编码组
     * @throws Exception    异常
     */
    public static void doAddReaders(HttpRequest request, String readerCodes) throws Exception
    {
        List<String> readerList = Lists.toStringList(readerCodes);
        for (String item : readerList)
        {
            GitReader reader = new GitReader();
            reader.setOperatorCode(request.getSessionName());
            reader.setReaderCode(item);
            
            ORM.get(ZTable.class, request).replace(reader);
        }
    }
    
    /**
     * 删除汇报对象
     * 
     * @param request       请求对象
     * @param projectId     项目编号
     * @param readerCode    汇报对象编码
     * @throws Exception    异常
     */
    public static void doDeleteReader(HttpRequest request, String readerCode) throws Exception
    {
        Selector selector = new Selector();
        selector.addMust("operatorCode", request.getSessionName());
        selector.addMust("readerCode", readerCode);
        
        ORM.get(ZTable.class, request).delete(GitReader.class, selector);
    }
    
    /**
     * 删除向我汇报的操作员
     * 
     * @param request       请求对象
     * @param projectId     项目编号
     * @param operatorCode  操作员编码
     * @throws Exception    异常
     */
    public static void doDeleteOperator(HttpRequest request, String operatorCode) throws Exception
    {
        Selector selector = new Selector();
        selector.addMust("readerCode", request.getSessionName());
        selector.addMust("operatorCode", operatorCode);
        
        ORM.get(ZTable.class, request).delete(GitReader.class, selector);
    }
}
