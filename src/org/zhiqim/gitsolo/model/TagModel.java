/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.model;

import org.zhiqim.git.GitConstants;
import org.zhiqim.git.objer.objectid.RevCommit;
import org.zhiqim.git.refer.Ref;
import org.zhiqim.kernel.util.DateTimes;

/**
 * 标签模型对象，用于HTTP组装对象
 *
 * @version v1.0.0 @author zouzhigang 2017-5-12 新建与整理
 */
public class TagModel implements GitConstants
{
    private Ref ref;
    private RevCommit commit;
    
    public String getName()
    {
        return ref.getName().substring(P_REFS_TAGS_.length());
    }
    
    public String getCommitAuthor()
    {
        return commit.getCommitterIdent().getName();
    }
    
    public String getCommitTime()
    {
        return DateTimes.toDateTimeString(commit.getCommitTime() * 1000L);
    }
    
    public Ref getRef()
    {
        return ref;
    }
    
    public TagModel setRef(Ref ref)
    {
        this.ref = ref;
        return this;
    }
    
    public RevCommit getCommit()
    {
        return commit;
    }
    
    public TagModel setCommit(RevCommit commit)
    {
        this.commit = commit;
        return this;
    }
}
