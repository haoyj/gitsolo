/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.dbo;

import java.io.Serializable;
import java.sql.Timestamp;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 项目动态表 对应表《GIT_REPORT》
 */
@AnAlias("GitReport")
@AnNew
@AnTable(table="GIT_REPORT", key="REPORT_ID", type="InnoDB")
public class GitReport implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="OPERATOR_CODE", type="string,32", notNull=true)    private String operatorCode;    //2.操作员编码
    @AnTableField(column="REPORT_ID", type="long", notNull=true)    private long reportId;    //3.动态编号
    @AnTableField(column="REPORT_TIME", type="datetime", notNull=true)    private Timestamp reportTime;    //4.动态时间
    @AnTableField(column="REPORT_DESC", type="string,200", notNull=false)    private String reportDesc;    //5.动态描述

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public String getOperatorCode()
    {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode)
    {
        this.operatorCode = operatorCode;
    }

    public long getReportId()
    {
        return reportId;
    }

    public void setReportId(long reportId)
    {
        this.reportId = reportId;
    }

    public Timestamp getReportTime()
    {
        return reportTime;
    }

    public void setReportTime(Timestamp reportTime)
    {
        this.reportTime = reportTime;
    }

    public String getReportDesc()
    {
        return reportDesc;
    }

    public void setReportDesc(String reportDesc)
    {
        this.reportDesc = reportDesc;
    }

}
