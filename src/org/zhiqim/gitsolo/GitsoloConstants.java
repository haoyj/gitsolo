/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo;

import org.zhiqim.httpd.context.ZmlContextConstants;
import org.zhiqim.kernel.annotation.AnAlias;

/**
 * Gitsolo项目常量定义表
 *
 * @version v1.0.0 @author zouzhigang 2017-10-14 新建与整理
 */
@AnAlias("GitsoloConstants")
public interface GitsoloConstants extends ZmlContextConstants
{
    /*****************************************************************************/
    //Git库提交&更新时的前缀
    /*****************************************************************************/
    
    /** Git前缀名称 */
    public static final String GIT_URL_PREFIX               = "/git";
    
    /*****************************************************************************/
    //权限获取成功&失败
    /*****************************************************************************/
    
    /** 初始 */
    public static final int GIT_RULE_INIT                   = 0;
    
    /** 成功 */
    public static final int GIT_RULE_SUCC                   = 1;
    
    /** 失败 */
    public static final int GIT_RULE_FAIL                   = 2;
    
    /*****************************************************************************/
    //操作员参数KEY
    /*****************************************************************************/
    
    /** 操作员独立密钥KEY */
    public static final String GIT_SECRET_KEY               = "git_secret";
    
    /*****************************************************************************/
    //项目存储KEY
    /*****************************************************************************/
    
    /** 操作员默认项目成员保存到会话中的KEY */
    public static final String GIT_MEMBER_KEY               = "git_member";
    
    /** 操作员存储到操作员参数字段的默认项目编号KEY */
    public static final String GIT_PROJECT_ID_KEY           = "git_project_id";
    
    /*****************************************************************************/
    //项目成员7种角色编码
    /*****************************************************************************/
    
    /** 管理 */
    public static final String FPM_ROLE_MANAGE              = "manage";
    
    /** 需求 */
    public static final String FPM_ROLE_DEMAND              = "demand";
    
    /** 观察 */
    public static final String FPM_ROLE_OBSERVER            = "observer";
    
    /** 设计 */
    public static final String FPM_ROLE_DESIGN              = "design";
    
    /** 开发 */
    public static final String FPM_ROLE_DEVELOPMENT         = "development";
    
    /** 测试 */
    public static final String FPM_ROLE_TEST                = "test";
    
    /** 运维 */
    public static final String FPM_ROLE_OPERATION           = "operation";
    
    
    /*****************************************************************************/
    //项目成员8种常用仓库编码
    /*****************************************************************************/
    
    /** 文档库 */
    public static final String FPM_REOP_DOCUMENT            = "document";
    
    /** 设计库 */
    public static final String FPM_REOP_DESIGN              = "design";
    
    /** 开发库 */
    public static final String FPM_REOP_DEVELOPMENT         = "development";
    
    /** 测试库 */
    public static final String FPM_REOP_TEST                = "test";
    
    /** 发布库 */
    public static final String FPM_REOP_RELEASE             = "release";
    
    /** 运维库 */
    public static final String FPM_REOP_PROJECT             = "operation";
    
    /** 管理库 */
    public static final String FPM_REOP_MANAGE              = "manage";
    
    /** 备份库 */
    public static final String FPM_REOP_BACKUP              = "backup";
}
