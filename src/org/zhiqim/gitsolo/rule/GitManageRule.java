/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.rule;

import org.zhiqim.gitsolo.GitsoloConstants;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.gitsolo.dbo.GitMember;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.rule.CheckRule;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnGlobal;
import org.zhiqim.kernel.util.Strings;
import org.zhiqim.kernel.util.mutables.MObj;
import org.zhiqim.manager.ZmrSessionUser;

/**
 * 页面验证是否有管理权限，返回boolean =true表示有，=false表示没有
 *
 * @version v1.0.0 @author zouzhigang 2017-10-16 新建与整理
 */
@AnAlias("GitManageRule")
@AnGlobal
public class GitManageRule implements CheckRule, GitsoloConstants
{
    public boolean check(HttpRequest request) throws Exception
    {
        int result = prefix(request, new MObj<GitMember>());
        
        switch (result)
        {
        case GIT_RULE_SUCC:  return true;
        case GIT_RULE_FAIL: return false;
        default: return false;
        }
    }
    
    public static int prefix(HttpRequest request, MObj<GitMember> member) throws Exception
    {
        ZmrSessionUser sessionUser = request.getSessionUser(ZmrSessionUser.class);
        if (sessionUser == null)
        {// 用户未登录或超时
            return GIT_RULE_FAIL;
        }
        
        if (sessionUser.isAdmin())
        {//管理员或超级管理员有所有项目权限
            return GIT_RULE_SUCC;
        }
        
        long projectId = sessionUser.getOperatorParamLong(GIT_PROJECT_ID_KEY);
        if (projectId == -1)
        {//未选中项目
            return GIT_RULE_FAIL;
        }
        
        GitMember mem = (GitMember)sessionUser.getValue(GIT_MEMBER_KEY);
        if (mem == null || mem.getProjectId() != projectId)
        {//有选中项目，但未加载项目对象到会话中时，主动加载一次
            GitProjectDao.setProjectMember(request, sessionUser, projectId);
            mem = (GitMember)sessionUser.getValue(GIT_MEMBER_KEY);
        }
        
        if (mem == null)
        {//重新获取也没有成员会话信息
            return GIT_RULE_FAIL;
        }
        
        if (mem.getMemberType() == 0 || Strings.contains(mem.getMemberRole(), FPM_ROLE_MANAGE))
        {//组长有所有权限
            return GIT_RULE_SUCC;
        }
        
        member.val(mem);
        return GIT_RULE_INIT;
    }
}
