/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.rule;

import org.zhiqim.gitsolo.GitsoloConstants;
import org.zhiqim.gitsolo.dbo.GitMember;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.rule.CheckRule;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnGlobal;
import org.zhiqim.kernel.util.Strings;
import org.zhiqim.kernel.util.mutables.MObj;

/**
 * 页面验证是否有开发或设计权限，返回boolean =true表示有，=false表示没有
 *
 * @version v1.0.0 @author zouzhigang 2017-10-16 新建与整理
 */
@AnAlias("GitDevDesignRule")
@AnGlobal
public class GitDevDesignRule implements CheckRule, GitsoloConstants
{
    public boolean check(HttpRequest request) throws Exception
    {
        MObj<GitMember> member = new MObj<>();
        int result = GitManageRule.prefix(request, member);
        
        switch (result)
        {
        case GIT_RULE_SUCC:  return true;
        case GIT_RULE_FAIL: return false;
        default: 
        {
            String role = member.value.getMemberRole();
            return Strings.contains(role, FPM_ROLE_DEVELOPMENT) ||
                    Strings.contains(role, FPM_ROLE_DESIGN);
        }
        }
    }
}
